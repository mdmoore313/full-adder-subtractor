//Michael Moore Project 2
//October 26, 2015
//Status: Fully Working

//Behavioral 1 bit adder Description
module  Moore_1badd (A, B, Cin, Sum, Cout);
	input A,B,Cin; //3 1-bit Inputs
	output reg Sum,Cout; //2 1-bit Outputs
	always @(A or B or Cin) 
	begin //reevaluate if these change
		Sum <= (A & ~B & ~Cin) | (~A & B) | (~A & Cin) | (B & Cin); //See truth table for Sum Output
		Cout <= (A & B) | (A & Cin); //See truth table for Carry Output
	end
endmodule

//Structural Description of 4 bit ripple adder
module Moore_4badd (A, B, Cout, Sout, SUB);
	input [3:0] A,B; //Two 4 bit inputs
	input c0, SUB;
	output [3:0] Sout; //Sum or difference of inputs
	output Cout;
	wire c1,c2,c3,c4; //Internal carry wires
	
	assign c0 = 0;
	assign Cout = c4;
	
	//Invert B input if SUB is high
	//assign B = SUB^B
	
	//Connect 4 individual 1 bit adders together
	//Toggle B input if the SUB input is HIGH
	Moore_1badd a0(A[0],SUB^B[0], c0 , Sout[0], c1);
	Moore_1badd a1(A[1],SUB^B[1], c1 , Sout[1], c2);
	Moore_1badd a2(A[2],SUB^B[2], c2 , Sout[2], c3);
	Moore_1badd a3(A[3],SUB^B[3], c3 , Sout[3], c4);
	
	

endmodule
 