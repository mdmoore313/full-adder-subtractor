//Michael Moore Project 2
//October 26, 2015
//Status: Fully Working

//Testbench
module Moore_add_tb;
reg [3:0] A, B, A_next, B_next;
output [3:0] Result;
output Carry;
reg clkA, clkB, Subtractor;
Moore_4badd A1(A,B,Carry , Result[3:0], Subtractor);

initial
begin
	$dumpfile("moore_proj2.vcd");
    $dumpvars(0,Moore_add_tb);
	clkA = 1;
	clkB = 0;
	A=4'b0000; B=4'b0000; //Set initial value of A and B to zero for testing
	A_next = 1;
	B_next = 1;
	Subtractor = 0;
end

//Two clocks for A and B, A will increment twice as fast as B
always begin
	#1 clkA =~ clkA;
	#2 clkB =~ clkB;
	#3 Subtractor = 1;
end

always @(posedge clkA) begin
	A = A_next;
	A_next = A + 1;
   B_next = B + 1;	
end

always @(posedge clkB) begin
	B = B_next;
end

initial
#175 $finish;
endmodule
